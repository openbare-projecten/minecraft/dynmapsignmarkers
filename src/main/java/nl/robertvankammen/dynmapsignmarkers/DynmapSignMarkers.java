package nl.robertvankammen.dynmapsignmarkers;

import com.destroystokyo.paper.event.block.BlockDestroyEvent;
import net.kyori.adventure.text.TextComponent;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.data.type.HangingSign;
import org.bukkit.block.data.type.Sign;
import org.bukkit.block.data.type.WallHangingSign;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.block.sign.Side;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.dynmap.DynmapAPI;
import org.dynmap.bukkit.DynmapPlugin;
import org.dynmap.markers.GenericMarker;
import org.dynmap.markers.MarkerAPI;
import org.dynmap.markers.MarkerIcon;
import org.dynmap.markers.MarkerSet;

import java.util.List;
import java.util.Optional;

import static java.util.UUID.randomUUID;

public class DynmapSignMarkers extends JavaPlugin implements Listener {
    private DynmapAPI dynmapAPI;
    private MarkerIcon baseIcon;
    private MarkerIcon portalIcon;
    private MarkerAPI markerAPI;

    private final List<Class> signClasses = List.of(WallSign.class, Sign.class, HangingSign.class, WallHangingSign.class);

    @Override
    public void onEnable() {
        var pluginManager = this.getServer().getPluginManager();
        pluginManager.registerEvents(this, this);
        dynmapAPI = (DynmapPlugin) pluginManager.getPlugin("dynmap");
        assert dynmapAPI != null;
        markerAPI = dynmapAPI.getMarkerAPI();
        baseIcon = markerAPI.getMarkerIcon("house");
        portalIcon = markerAPI.getMarkerIcon("portal");
    }

    @EventHandler
    public void onPlace(SignChangeEvent event) {
        var location = event.getBlock().getLocation();
        var commandLine = ((TextComponent) event.line(0)).content();
        var baseName = ((TextComponent) event.line(1)).content();

        //try to remove first because of edit of signs
        removeMarker(location, commandLine);

        var iconName = checkFirstLine(commandLine);
        iconName.ifPresent(markerIcon -> getOrCreateMarkerSet(markerIcon)
                .createMarker(randomUUID().toString(), baseName,
                        location.getWorld().getName(), location.getBlockX(), location.getBlockY(), location.getBlockZ(), markerIcon, true));
    }

    @EventHandler
    public void onDestroy(BlockBreakEvent blockBreakEvent) {
        destroySign(blockBreakEvent.getBlock());
    }

    @EventHandler
    public void onDestroy(BlockDestroyEvent blockDestroyEvent) {
        destroySign(blockDestroyEvent.getBlock());
    }

    private void destroySign(Block block) {
        var blockMaterialData = block.getBlockData().getMaterial().data;
        if (signClasses.contains(blockMaterialData)) {
            // beide kanten verwijderen
            var commandLine = ((TextComponent) ((org.bukkit.block.Sign) block.getState()).getSide(Side.FRONT).line(0)).content();
            removeMarker(block.getLocation(), commandLine);
            commandLine = ((TextComponent) ((org.bukkit.block.Sign) block.getState()).getSide(Side.BACK).line(0)).content();
            removeMarker(block.getLocation(), commandLine);
        }
    }

    private Optional<MarkerIcon> checkFirstLine(String firstLine) {
        switch (firstLine) {
            case "Base", "base", "Home", "home" -> {
                return Optional.of(baseIcon);
            }
            case "portal", "Portal" -> {
                return Optional.of(portalIcon);
            }
            default -> {
                return Optional.empty();
            }
        }
    }

    private MarkerSet getOrCreateMarkerSet(MarkerIcon markerIcon) {
        var markerSet = markerAPI.getMarkerSet(markerIcon.getMarkerIconLabel());
        if (markerSet == null) {
            markerSet = markerAPI.createMarkerSet(markerIcon.getMarkerIconLabel(), markerIcon.getMarkerIconLabel(), null, true);
        }
        return markerSet;
    }

    private void removeMarker(Location location, String commandLine) {
        checkFirstLine(commandLine).flatMap(markerIcon -> getOrCreateMarkerSet(markerIcon).getMarkers().stream()
                .filter(marker -> marker.getX() == location.getX())
                .filter(marker -> marker.getY() == location.getY())
                .filter(marker -> marker.getZ() == location.getZ())
                .filter(marker -> marker.getWorld().equals(location.getWorld().getName()))
                .findAny()).ifPresent(GenericMarker::deleteMarker);
    }
}
